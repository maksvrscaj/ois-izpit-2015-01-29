var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));

var uporabniki=require('./public/podatki/uporabniki_streznik.json');

var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Zunanja avtentikacija)
 */
app.get('/api/prijava', function(req, res) {
	uporabniskoIme=req.param('uporabniskoIme');
	geslo=req.param('geslo');
	console.log(uporabniskoIme);
	var imaPravico=false;
	if(preveriSpomin(uporabniskoIme,geslo))imaPravico=true;
	else if(preveriDatotekaStreznik(uporabniskoIme,geslo))imaPravico=true;
	if(imaPravico){
		console.log('ima pravico');
		res.send({status:true,napaka: ""});
	}else if(uporabniskoIme === undefined || geslo===undefined || uporabniskoIme == '' || geslo ==''){
		console.log('nima pravice undefined');
		res.send({status: false,napaka:"Napacna zahteva"});
	}
	else{
		console.log('nima pravice else');
		res.send({status:false,napaka:"Avtenkacija ni uspela"});
	}

});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
 */
app.get('/prijava', function(req, res) {
	uporabniskoIme=req.param('uporabniskoIme');
	geslo=req.param('geslo');
	var imaPravico=false;
	if(preveriSpomin(uporabniskoIme,geslo))imaPravico=true;
	else if(preveriDatotekaStreznik(uporabniskoIme,geslo))imaPravico=true;
	if(imaPravico){
		var string="<html><title>Uspešno</title><body><p>Uporabnik <b>"+uporabniskoIme+"</b> uspešno prijavljen v sistem</p></body></html>";
		res.send(string);

	}
	else{
		var string="<html><title>Napaka</title><body><p>Uporabnik <b>"+uporabniskoIme+"</b> nima pravice prijave v sistem!</p></body></html>"
		res.send(string);

	}

	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (branje datoteka na strani strežnika)
 */
var podatkiDatotekaStreznik = {};


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriSpomin(uporabniskoIme, geslo) {
	var retVal=false;
	podatkiSpomin.forEach(function(item){
		var user=item.split('/');
		if(uporabniskoIme === user[0] && geslo=== user[1])retVal=true;
	});
	return retVal;
	// ...
}


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	var retVal=false;
	uporabniki.forEach(function(item){
		var uName=item.uporabnik;
		var pass=item.geslo;
		if(uporabniskoIme === uName && geslo===pass)retVal=true;
	});
	return retVal;
	// ...
}